# websocketFuzzer

Fuzzing WebSocket Requests.

```
service apache2 stop
service docker restart
docker run -d -p 80:80 -p 8080:8080 tssoffsec/dvws
```

```
└──╼ #cat /etc/hosts
127.0.0.1	localhost
127.0.1.1	kali

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

192.168.0.106 dvws.local

```

http://dvws.local/error-sql-injection.php

let's try script on above url:
passing username and password, "Outgoing" message in base64 converted `auth_user` and `auth_pass`

```json
{"auth_user":"YWFh","auth_pass":"YmJi"}
```

Now use `socket2http.py` 

```
cat message.txt 
{"auth_user":"YWFh","auth_pass":"[FUZZ]"}

python socket2http.py -u "ws://dvws.local:8080/authenticate-user" -m message.txt
Started httpserver on port  8000
```

it opens a loopback http server which communicates with websocket at dvws.local:8080 in the behind. in order to fuzz the requests make the following request:


```
curl "http://localhost:8000/?fuzz=xxxxxx"
<pre>Table 'dvws_db.users' doesn't exist</pre>

└──╼ #curl "http://localhost:8000/?fuzz=xxxx'"
<pre>You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''xxxx''' at line 1</pre>
```


Now we can use this request to burp intruder,sqlmap,commix etc

>>> TODOS:

1. Implement this in cimex fuzzer
2. Burp Plugin.
etc...

https://www.vdalabs.com/2019/03/05/hacking-web-sockets-all-web-pentest-tools-welcomed/